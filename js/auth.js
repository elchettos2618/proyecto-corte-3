import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import app from "./firebaseConfig.js";

const btnLogin = document.getElementById('btnLogin');

btnLogin.addEventListener("click", (e) =>  {
    e.preventDefault();

    const txtEmail = document.getElementById('txtEmail').value;
    const txtPassword = document.getElementById('txtPassword').value;

    const auth = getAuth(app);
    signInWithEmailAndPassword(auth, txtEmail, txtPassword)
    .then((userCredential) => {
        // Signed in 
        const user = userCredential.user;

        alert(`Logeado con: ${user.email}`)
        window.location.href = "/html/menu.html";

        // ...
    })
    .catch((error) => {
        const errorCode = error.code;
        alert(`cagada monumental`)
    });
});
