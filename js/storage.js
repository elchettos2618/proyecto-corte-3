
import { getStorage, ref, uploadBytes, getDownloadURL  } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

import app from ".//firebaseConfig.js";

export const storage = getStorage(app);

export const storageRef = ref(storage);

export const imageRef = ref(storage, 'imagenes/');


export function uploadFiles(ref ,file){

    uploadBytes(ref, file).then((snapshot) => {
        console.log("Se subió");
    });

}

export function downloadFiles(reference, route){
    getDownloadURL(ref(reference, route, image))
    .then((url) => {
        const img = document.getElementById(image);
        img.setAttribute('src', url);

    })
    .catch((error) =>{
        alert("no jalo");
    });
}

