import { getDatabase, onValue, ref } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

import app from "./firebaseConfig.js";

document.addEventListener("DOMContentLoaded", function () {
    mostrarProductos();
  });

let container = document.getElementById('container-producto');

function mostrarProductos() {
    const db = getDatabase();
    const dbRef = ref(db, 'articulos');
    onValue(dbRef, (snapshot) => {
        container.innerHTML = "";  // Limpia el contenido actual del contenedor
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            // Utiliza el operador de concatenación (+=) para agregar contenido al contenedor
            container.innerHTML += "<div class='arti'>" + " " + childData.nombre + " $" + childData.precio + " " + "<div>";
        });
    }, {
        onlyOnce: true
    });
}



const btnLogin = document.getElementById('btnLogin');

btnLogin.addEventListener('click', (e) => {
    e.preventDefault();
    window.location.href = "./html/login.html"; s
});

