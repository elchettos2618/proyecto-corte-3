
import { storage, uploadFiles, imageRef } from "./storage.js";
import { ref } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

const imageInput = document.getElementById('customFile');
const image = document.getElementById('imagen');
const btnSubir = document.getElementById('btnSubir');
var fileName = "";

// muestra imagen subida
imageInput.addEventListener('change', function() {

    if (imageInput.files.length > 0) {
        fileName = imageInput.files[0].name;
        const file = imageInput.files[0];
        const objectURL = URL.createObjectURL(file);

        image.src = objectURL;
    } else {
        image.src = ' ';
    }
});


// Valida si termina en una extensión valida
function filtrarExtension(nombre){
    const minuscula = nombre.toLowerCase();
    return minuscula.endsWith(".jpeg") || minuscula.endsWith(".jpg") || minuscula.endsWith(".png");
};


btnSubir.addEventListener('click', function() {

    if (filtrarExtension(fileName)){
        const fileNameRef = ref(storage, 'imagenes/' + fileName);
        const file = imageInput.files[0];
        uploadFiles(fileNameRef,file);
    }
    else {
        alert("Tipo de archivo no valido");
    }
});

