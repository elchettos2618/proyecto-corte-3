import { getDatabase, ref, set, get, child, onValue} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import app from "./firebaseConfig.js";

const database = getDatabase(app);

export function writeUserData(id, code, name, precio, estado, imageUrl) {
    const db = getDatabase();
    return set(ref(db, 'articulos/' + id), {
        codigo: code,
        nombre: name,
        precio: precio,
        status: estado,
        url: imageUrl
    })
    .then(() => {
        console.log("Datos guardados exitosamente");
    })
    .catch((error) => {
        console.error("Error al guardar datos:", error);
        throw error; // Lanza el error para que sea manejado por la llamada
    });
}

export function readUserData(userId) {
    const dbRef = ref(getDatabase());

    return get(child(dbRef, `articulos/${userId}`)).then((snapshot) => {
        if (snapshot.exists()) {
            const userData = snapshot.val();
            return userData;
        } else {
            console.log("No hay datos disponibles");
            return null; // Devuelve null o algún valor predeterminado en caso de no existir datos
        }
    }).catch((error) => {
        console.error("Error al leer datos:", error);
        throw error; // Lanza el error para que sea manejado por la llamada
    });
};

export function idCount(){
  const dbRef = ref(getDatabase());

  return get(child(dbRef, `articulos`)).then((snapshot) => {
      if (snapshot.exists()) {

          const userData = snapshot.val();
          console.log(userData);
          return userData;
      } else {
          console.log("No hay datos disponibles");
          return null; // Devuelve null o algún valor predeterminado en caso de no existir datos
      }
  }).catch((error) => {
      console.error("Error al leer datos:", error);
      throw error; // Lanza el error para que sea manejado por la llamada
  });
};



/*
const btnCalar = document.getElementById('btnCalar');

btnCalar.addEventListener("click", async (e) => {
    e.preventDefault();

    try {
        const userData = await idCount();

        if (userData) {
            const largo = userData.length;
            console.log(largo);
        } else {
            console.log("No se encontraron datos para el usuario");
        }
    } catch (error) {
        console.error(error);
    }
});


*/





