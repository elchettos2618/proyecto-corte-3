// Import the functions you need from the SDKs you need

import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration

const firebaseConfig = {

  apiKey: "AIzaSyB6JncdRYwdhG7o8k5eEkWtXd0GclZjmss",

  authDomain: "administrador-web-88bbc.firebaseapp.com",

  databaseURL: "https://administrador-web-88bbc-default-rtdb.firebaseio.com",

  projectId: "administrador-web-88bbc",

  storageBucket: "administrador-web-88bbc.appspot.com",

  messagingSenderId: "38621075584",

  appId: "1:38621075584:web:6936340dfb2a7f5b3fae65"

};


// Initialize Firebase

const app = initializeApp(firebaseConfig);

export default app;